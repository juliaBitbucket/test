<?php

    $result = uploadFile();

    function uploadFile()
    {
        $fileFolder = __DIR__ . '/tests/';
        $fileName = 'test_' . time() . '.json';

        if (empty($_FILES)) {
            return array();
        }

        if (empty($_FILES['test']['name'])) {
            return array('error' => 'Выберите файл.');
        }

        if (!is_dir($fileFolder)) {
            mkdir($fileFolder);
        }

        $data = json_decode(file_get_contents($_FILES['test']['tmp_name']), true);
        if (!is_array($data)) {
            return array('error' => 'Данные в файле не корректные.');
        }

        if (!checkFormatData($data)) {
            return array('error' => 'Формат данных в файле не правильный.');
        }

        if (!move_uploaded_file($_FILES['test']['tmp_name'], $fileFolder . $fileName)) {
            return array('error' => 'Could not upload file.');
        } else {
            return array('success' => 'Файл загружн удачно!');
        }
    }

    function checkFormatData($data)
    {
        foreach ($data as $test) {
            if (!array_key_exists('question', $test) || !array_key_exists('variants', $test) || !array_key_exists('correct', $test)) {
                return false;
            }
            if (empty($test['question']) || empty($test['variants']) || empty($test['correct'])) {
                return false;
            }
            // check exist correct variant
            if (empty($test['correct']) || !array_key_exists($test['correct'], $test['variants'])) {
                return false;
            }
        }
        return true;
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Загрузка файлов</title>
    <style type="text/css">
        body {
            margin: 0px;
        }
        #downloadFile {
            margin: 30px;
        }
        #file {
            margin-bottom: 20px;
        }
        .msg {
            font-weight: bold;
            font-size: 20px;
        }
        .success {
            color: blue;
        }
        .error {
            color: red;
        }
        #menu {
            background: burlywood;
            height: 30px;
            padding: 20px 0px 0px 20px;
        }
        #menu a {
            text-decoration: none;
            margin-right: 10px;
        }
        #container {
            margin-left: 20px;
        }
    </style>
</head>
    <body>
        <div id="menu">
            <a class="menu" href="admin.php">Загрузить файл</a>
            <a class="menu" href="list.php">Список тестов</a>
        </div>

        <div id="container">
            <h2>Выберите файл с тестом</h2>
            <form enctype="multipart/form-data" method="post" name="testForm">
                <div id="downloadFile">
                    <div id="file" >
                        <input type="file" name="test"><br>
                    </div>
                    <input type="submit" value="Отправить">
                </div>
            </form>
            <span class="msg success"><?php echo isset($result['success']) ? $result['success'] : ''; ?></span>
            <span class="msg error"><?php echo isset($result['error']) ? 'ОШИБКА: ' . $result['error'] : ''; ?></span>

            <div>
                <p>Пример формата данных:</p>
            <span>
                [
                  {
                    "question":"What is the result of the following expression? 5+2*4+6",<br>
                    "variants":{<br>
                        "a":70,<br>
                        "b":19,<br>
                        "c":34,<br>
                        "d":21<br>
                    },<br>
                    "correct":"b"<br>
                  },<br>
                ...
                ]
            </span>
            </div>
        </div>
    </body>
</html>