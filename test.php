<?php
    $fileFolder = __DIR__ . '/tests/';
    if (!file_exists($fileFolder . $_GET['fileName'])) {
        echo "Файл ". $_GET['fileName'] . " не существует.";
        exit;
    }


    if (empty($_GET['fileName'])) {
        echo 'Непредвиденная ошибка';
        exit;
    }


    $tests = json_decode(file_get_contents($fileFolder . $_GET['fileName']), true);

    if (empty($tests)) {
        echo 'Непредвиденная ошибк';
        exit;
    }

    if (!empty($_POST['variant'])) {
        $correct = $error = 0;
        foreach ($_POST['variant'] as $key => $variant) {
            if ($tests[$key]['correct'] == $variant) {
                $correct++;
            } else {
                $error++;
            }
        }
        $result = array('correct' => $correct, 'error' => $error);
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset=utf-8">
    <title>Тест</title>
    <style type="text/css">
        body {
            margin: 0px;
        }
        #menu {
            background: burlywood;
            height: 30px;
            padding: 20px 0px 0px 20px;
        }
        #menu a {
            text-decoration: none;
            margin-right: 10px;
        }
        #container {
            margin-left: 20px;
        }
    </style>
</head>
    <body>
        <div id="menu">
            <a class="menu" href="admin.php">Загрузить файлe</a>
            <a class="menu" href="list.php">Список тестов</a>
        </div>

        <div id="container">
            <?php if (!isset($result)): ?>
            <div>
                <h3>Тест</h3>
                <form enctype="multipart/form-data" method="post" name="testForm">
                    <?php foreach ($tests as $i => $test): ?>
                        <h4><?php echo $test['question']; ?></h4>
                        <?php foreach ($test['variants'] as $key => $variant): ?>
                            <input required type="radio" name="variant[<?php echo $i; ?>]" value="<?php echo $key; ?>">
                            <span><?php echo $variant; ?></span><br>
                        <?php endforeach; endforeach; ?>
                    <input type="submit" value="Отправить">
                </form>
            </div>

            <?php else: ?>
                <div>
                    Тест пройден.<br>
                    Правильных ответов - <?php echo $result['correct']; ?>;<br>
                    Неправильных ответов - <?php echo $result['error']; ?>.
                </div>
            <?php endif; ?>
        </div>
    </body>
</html>
